import os
os.system('clear')

op=1
lista=[]

while True:
    num=int(input("digite um numero: "))
    lista.append(num)



    op=input('deseja continuar? (s/n): ')
    if op in 'nN':
        break

print(f'A lista é {lista} ')
print('-'*30)

print(f'A lista tem {len(lista)} elementos ')
print('-'*30)

print(f' lista invertida é {lista[::-1]}  ')
print('-'*30)

lista.sort()
print(f'A lista sorteada é {lista} ')
print('-'*30)

print(f' ultimo item da lista {lista[-1]}  ')
print('-'*30)

lista.insert(2,5)
print(f' inserindo o numero cinco na lista no indic dois: {lista}')

print('-'*30)
print(f' elementos entre as posicoes 2 e 4: {lista[2:5]}  ')


